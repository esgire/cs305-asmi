package shape_calculator;

public class calculator {   
     public static void main(String arg[]) {    
          Circle circle = new Circle(10.0);    
          Triangle triangle = new Triangle(10.0, 10.0);
          Square square = new Square(10.0);
          Rectangle rect = new Rectangle(10.0, 10.0);
          Rectangle cube = new Cube(10.0);
          System.out.println("\nArea of Circle: " + circle.getArea());    
          System.out.println("\nArea of Square: " + square.getArea());
          System.out.println("\nArea of Rectangle: " + rect.getArea());    
          System.out.println("\nArea of Triangle: " + triangle.getArea());    
          System.out.println("\nArea of Cube: " + cube.getArea()); 
        }    
  }  