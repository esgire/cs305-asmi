package shape_calculator;

public class Cube extends Rectangle {
    
    public Cube(double x){
        super(x, x);
    }
    
    public double getArea(){
        return super.getArea() * 6;
    }
}