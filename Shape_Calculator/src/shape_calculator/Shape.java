package shape_calculator;

public interface Shape {
	  double getArea();
}