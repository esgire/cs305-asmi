package shape_calculator;

public class Rectangle implements Shape {
    private double x;
    private double y;
    
    public Rectangle(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public double getArea() {    
      return (this.x * this.y); 
    }
}