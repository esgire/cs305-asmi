package calculator;

public class BinaryOpSub extends BinaryOp {

	public BinaryOpSub( Double n1, Double n2) {
		super( n1, n2);
		// TODO Auto-generated constructor stub
	}
	
	public Double calc() {
		return getN1() - getN2();
	}
}
