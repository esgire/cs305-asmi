package calculator;

public class BinaryOpPow extends BinaryOp {

	public BinaryOpPow( Double n1, Double n2) {
		super( n1, n2);
		// TODO Auto-generated constructor stub
	}
	
	public Double calc() {
		return Math.pow(getN1(), getN2());
	}
}
