package calculator;

import java.util.Scanner;

public class Calculator {
	public static void main (String[] args) {
        double num1 = 0.0;
        double num2 = 0.0;
        String operation;
        String option;
		
		Scanner input = new Scanner(System.in);

		System.out.println("Ternary (T) or Binary (B): ");
		option = input.next();
		
        System.out.println("Please enter first number: ");
        num1 = input.nextInt();

        if(option.equals("B")) {
	        System.out.println("Please enter second number: ");
	        num2 = input.nextInt();
        }
        
        Scanner op = new Scanner(System.in);
        System.out.println("Please enter operator: ");
        operation = op.next();

        if (operation.equals("+")){
        	BinaryOpAdd n = new BinaryOpAdd( num1, num2);
            System.out.println("Your answer is: " + n.calc());
        } if (operation.equals("-")) {
        	BinaryOpSub n = new BinaryOpSub( num1, num2);
            System.out.println("Your answer is: " + n.calc());
        } if (operation.equals("/")) {
        	BinaryOpDivide n = new BinaryOpDivide( num1, num2);
            System.out.println("Your answer is: " + n.calc());;
        } if (operation.equals("*")) {
        	BinaryOpMult n = new BinaryOpMult( num1, num2);
            System.out.println("Your answer is: " + n.calc());
        } if (operation.equals("p")) {
        	BinaryOpPow n = new BinaryOpPow( num1, num2);
            System.out.println("Your answer is: " + n.calc());
        } if (operation.equals("sin")) {
        	TernaryOpSin n = new TernaryOpSin( num1);
            System.out.println("Your answer is: " + n.calc());
        } if (operation.equals("cos")) {
        	TernaryOpCos n = new TernaryOpCos( num1);
            System.out.println("Your answer is: " + n.calc());
        } if (operation.equals("tan")) {
        	TernaryOpTan n = new TernaryOpTan( num1);
            System.out.println("Your answer is: " + n.calc());
        }
        

	}
}
