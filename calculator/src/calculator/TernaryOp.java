package calculator;

public class TernaryOp {
	private double n;
	
	public TernaryOp(double n) {
		this.setN(n);
	}
	
    public Double calc() {
        return n;
    }

	public double getN() {
		return n;
	}

	public void setN(double n) {
		this.n = n;
	}

}